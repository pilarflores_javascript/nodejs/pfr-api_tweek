export interface Task {
  
    description: string;
  
    date?: Date;
  
    isComleted?: boolean;
  
    color?: string;
}
