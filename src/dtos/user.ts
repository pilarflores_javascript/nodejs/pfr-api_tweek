
export interface User {

    username: string;
  
    email: string;
  
    isVerified?: boolean;
    
    firstname?: string;
   
    lastname?: string;
    
    password?: string;
}
