import { Router, Request, Response } from 'express';
import Task from '../models/task';

const router = Router();

router.get('/tasks', (req: Request, res: Response) => {

    res.json( {
        ok: true,
        message: 'Todo va bien'
    });

});

router.post('/tasks', (req: Request, res: Response) => {

    Task.findAll({
        attributes: [
          'id',
          'description'
        ]
      })
        .then(tasks => {
          res.status(200).json(tasks);
        })
    


    // res.json( {
    //     ok: true,
    //     message: 'Creando'
    // });

});

router.get('/tasks/:id', (req: Request, res: Response) => {

    const id = req.params.id;

    res.json( {
        ok: true,
        message: 'Buscando',
        id
    });

});

router.put('/tasks/:id', (req: Request, res: Response) => {

    const id = req.params.id;

    res.json( {
        ok: true,
        message: 'Actualizando',
        id
    });


});

router.delete('/tasks/:id', (req: Request, res: Response) => {

    const id = req.params.id;

    res.json( {
        ok: true,
        message: 'Borrando',
        id
    });

});




export default router;


