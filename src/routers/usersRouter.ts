import { Router, Request, Response } from 'express';

const router = Router();

router.get('/users', (req: Request, res: Response) => {

    res.json( {
        ok: true,
        message: 'Todo va bien'
    });

});

router.post('/users', (req: Request, res: Response) => {

    res.json( {
        ok: true,
        message: 'Creando'
    });

});

router.get('/users/:id', (req: Request, res: Response) => {

    const id = req.params.id;

    res.json( {
        ok: true,
        message: 'Buscando',
        id
    });

});

router.put('/users/:id', (req: Request, res: Response) => {

    const id = req.params.id;

    res.json( {
        ok: true,
        message: 'Actualizando',
        id
    });


});

router.delete('/users/:id', (req: Request, res: Response) => {

    const id = req.params.id;

    res.json( {
        ok: true,
        message: 'Borrando',
        id
    });

});




export default router;


