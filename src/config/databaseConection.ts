import { Client } from 'pg';

export default class DataBaseConection {

    private static _instance: DataBaseConection;
    client: Client;

    constructor (){
        this.client = new Client({
            user: 'postgres',
            host: 'localhost',
            database: 'tasksdb',
            password: 'root',
            port: 5433,
        });
        this.connect();
    }

    public static get instance(){
        return this._instance || (this._instance = new this());
    }

    private connect(){
        this.client.connect((error: Error) => {
            if(error) {
                console.error(error.message);
                return;
            }
            console.log(`Established connection`);
        });
    }
}