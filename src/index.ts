import Server from './servers/server';
import tasksRouter from './routers/tasksRouter';
import usersRouter from './routers/usersRouter';
import loginRouter from './routers/loginRouter';
import DataBaseConection from './config/databaseConection';

const server = Server.init(3000);

server.app.use(tasksRouter, usersRouter, loginRouter);

server.start( () => console.log(`Listening on port 3000.`))
 
DataBaseConection.instance;




