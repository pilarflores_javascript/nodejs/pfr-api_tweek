import { Sequelize, Model, DataTypes } from 'sequelize';

const sequelize = new Sequelize('sqlite::memory');

class Color extends Model {}
Color.init({
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
	primaryKey: true,
  },
  description: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true,
    },
  codeHex: {
      type: DataTypes.STRING,
      unique: true,
    },
    
}, {
    sequelize, // We need to pass the connection instance
    modelName: 'colors' // We need to choose the model name
  });
  
  
  // the defined model is the class itself
  console.log(Color === sequelize.models.Color); // true

  export default Color;