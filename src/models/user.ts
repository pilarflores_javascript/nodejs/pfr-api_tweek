import { Sequelize, Model, DataTypes } from 'sequelize';

const sequelize = new Sequelize('sqlite::memory');

class User extends Model {}

User.init({
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  username: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true,
      validate: {
        // We require usernames to have length of at least 3, and
        // only use letters, numbers and underscores.
        is: /^\w{3,}$/,
        }
    },
  email: {
      type: DataTypes.STRING,
      unique: true,
    },
    // isVerified: {
    //   type: DataTypes.BOOLEAN,
    //   allowNull: false,
    // },
    firstname: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    lastname: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    password: {
      type: DataTypes.DATE,
      allowNull: false,
    },
}, {
    sequelize, // We need to pass the connection instance
    modelName: 'users' // We need to choose the model name
  });
  
  
  // the defined model is the class itself
  console.log(User === sequelize.models.User); // true

  export default User;