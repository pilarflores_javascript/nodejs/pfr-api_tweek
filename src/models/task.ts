import { Sequelize, Model, DataTypes } from 'sequelize';
import Color from './color';
import User from './user';

const sequelize = new Sequelize('sqlite::memory');

class Task extends Model {}

Task.init({
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
		primaryKey: true,
  },
    description: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    is_completed: {
      type: DataTypes.BOOLEAN
    },
    color: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    creation_date: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    update_date: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    user: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    week_number: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: 'tasks' // We need to choose the model name
  });
  
//  Task.hasOne(User);
//  Task.hasMany(Color);
// Gardener.hasOne(Plot, {foreignKey: 'gardener_id'})
// https://medium.com/@paul.ebreo/sequelize-relationships-and-magic-methods-fb659c39a34d

Task.schema("tasks_scheme");
  // the defined model is the class itself
  console.log(Task === sequelize.models.Task); // true


  export default Task;