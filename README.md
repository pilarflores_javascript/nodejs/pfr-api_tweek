# INTRODUCTION


## Technologies


    - NodeJS
    - Swagger 
    - Express
    - Body-parser
    - Nodemon
    - Jwt-simple
    - Sequelize
    - PostgreSQL
    - Docker

## Database Model
![Data Base Model](./resources/images/png/database_model.png)



## Authentication
![Authentication Schema](/resources/images/png/security_schema.png)




## Swagger 


## Project Structure
    src
        config => :arrow_right: It contains the internal configurations
        controller => It contains database operations
        dtos => It constais dto entities
        middlewares
            authentication => It contains check if the token is valid
            validations => It contains validations of request bodies and request params
        models => It contains models of database
        routes => It contains paths
        services => It contais the business logic


